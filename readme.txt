=== BeautySpa ===
Contributors: weblizar
Tags: one-column, three-columns, custom-menu, custom-logo, custom-background, custom-header, right-sidebar, theme-options, threaded-comments, footer-widgets, featured-images, full-width-template, blog, portfolio , e-commerce
Requires at least: 4.9
Tested up to: 5.5
Requires PHP: 7.0
Stable tag: 1.7
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

BeautySpa is a free WordPress theme best for spas, beauty salon sites.

== Description ==
Beautyspa is a massage center, beauty spa salon responsive WordPress theme which caters to beauty salons, spas, massage, health and hair salons , Yoga, Wellness Center and any other health business purpose. . Compatible with popular plugins like WooCommerce and Contact form 7. It consists of homepage sections and easy to change link color.

<a href="https://wordpress.org/themes/beautyspa/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/beautyspa"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

= Where can I read more about BeautySpa? =

<a href="https://wordpress.org/themes/beautyspa/">Theme detail page</a>

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in BeautySpa WordPress Theme
-------------------------------------------------------------------------------------------------

1) Bootstrap
=================================================================================================
Bootstrap v3.3.5 (http://getbootstrap.com)
Copyright 2011-2015 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

2) Font Awesome
=================================================================================================
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

3) Swiper
=================================================================================================
Swiper 3.3.1
http://www.idangero.us/swiper/
Copyright 2016, Vladimir Kharlampidi
Licensed under MIT

4) jQuery Parallax
=================================================================================================
jQuery Parallax
Version 1.1.3
Author: Ian Lunn
Author URL: http://www.ianlunn.co.uk/
Plugin URL: http://www.ianlunn.co.uk/plugins/jquery-parallax/
Dual licensed under the MIT and GPL licenses:
http://www.opensource.org/licenses/mit-license.php

4) Photobox
=================================================================================================
photobox v1.9 (https://github.com/yairEO/photobox)
(c) 2013 Yair Even
License: MIT and GNU AGPLv3.(https://github.com/yairEO/photobox/blob/master/LICENSE)

== Screenshots ==

Slider image--
Source:https://pxhere.com/en/photo/1445991

Feature images--
Source:https://pxhere.com/en/photo/764487
Source:https://pxhere.com/en/photo/1206142
Source:https://pxhere.com/en/photo/764467

Header image--
Source:https://pxhere.com/en/photo/1060664

Copyright 2019, Pxhere
https://pxhere.com
Pxhere provides images under license
license: https://pxhere.com/en/license

== Changelog ==

=1.8 =
* Add Accessebility
* Update according to new guidline

=1.7 =
* Remove Inline CSS & Js
* Screenshot updated
* Minor CSS corrections.
* Changes in theme info notice.

= 1.6 =
* minor changes in css
* fixed text overflow

= 1.5.9 =
* Changes in theme info notice.

= 1.5.8 =
* Readme.text file changed as per new rule.
* Minor changes in theme info notice.
* Blog category option added.

= 1.5.7 =
* Screen-shot updated.
* Pro Banner added.

= 1.5.6 =
* Excerpt option added.
* Get Beautyspa Pro option added in customizer.

= 1.5.5 =
* Setup Home Page.
* Upgrade to Pro.
* wp_link_page() in a correct way.
* Rating banner added.

= 1.5.4 =
* released.