<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage beautyspa
 * @since BeautySpa 1.0
 */
?>
<!-- Footer start -->
<?php 
if ( is_active_sidebar( 'footer-widget-area' ) ){ ?>
	<div class="container-fluid space footer">
		<div class="container">
		 	<?php dynamic_sidebar( 'footer-widget-area' ); ?>
		</div>
	</div>
<?php }  ?>
	
<div class="container-fluid footer-bottom">
	<div class="container">
		<div class="row footer-copyright">
			<div class="col-md-9 col-sm-6 f_copyright">
				<?php 
				$beauty_options_footer_text = get_theme_mod('beauty_options_footer_text');
				if($beauty_options_footer_text !=''){ 
					echo esc_html(get_theme_mod('beauty_options_footer_text'));
					if(get_theme_mod('beauty_options_footer_link_text')!=''){ ?>
					 	<a class="social1" href="<?php echo esc_url(get_theme_mod('beauty_options_footer_link')); ?>" target="_blank" ><?php echo esc_html(get_theme_mod('beauty_options_footer_link_text')); ?></a>
					<?php }			
				} else { ?>
					<div><?php esc_html_e('&copy; 2020 Theme Preview. All Rights Reserved.','beautyspa');?> </div>
				<?php } ?>
			</div>
			<?php 
			$beauty_options_social_footer = get_theme_mod('beauty_options_social_footer');
			if($beauty_options_social_footer){ ?>
				<div class="col-md-3 col-sm-6 f_social">
					<div class="right-align">
						<ul class="social">
							<?php 
							$beauty_spa_facbook_link = get_theme_mod('beauty_spa_facbook_link');
							if($beauty_spa_facbook_link !=''){ ?>
								<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('beauty_spa_facbook_link')); ?>"><i class="fa fa-facebook"></i></a></li>
							<?php }
							$beauty_spa_twitter_link = get_theme_mod('beauty_spa_twitter_link');
							if($beauty_spa_twitter_link !=''){ ?>
								<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('beauty_spa_twitter_link')); ?>"><i class="fa fa-twitter"></i></a></li>
							<?php } 
							$beauty_spa_youtube_link = get_theme_mod('beauty_spa_youtube_link');
							if($beauty_spa_youtube_link !=''){ ?>
								<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('beauty_spa_youtube_link')); ?>"><i class="fa fa-youtube"></i></a></li>
							<?php } 
							$beauty_spa_linkdin_link = get_theme_mod('beauty_spa_linkdin_link');
							if($beauty_spa_linkdin_link !=''){ ?>
								<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('beauty_spa_linkdin_link')); ?>"><i class="fa fa-linkedin"></i></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Footer End -->
</div></div>
<?php wp_footer();?>
</body>
</html>