<?php 
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage beautyspa
 * @since BeautySpa 1.0
 */
if ( is_active_sidebar( 'sidebar-primary' ) ) { ?>
	<div class="col-md-3 left-sidebar ">
		<?php dynamic_sidebar( 'sidebar-primary' ); ?>
	</div>
<?php } ?>