<?php
/**
 * Custom Header functionality for beautyspa
 *
 * @package BeautySpa
 * @subpackage beautySpa
 * Sample implementation of the Custom Header feature.
 *
 * You can add an optional custom header image to header.php like so ...
 *
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses beautyspa_header_style()
 */
function beautyspa_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'wp_news_custom_header_args', array(
		'default-image'          => get_template_directory_uri() . '/images/3.jpg',
		'default-text-color'     => 'fff',
		'width'                  => 1200,
		'height'                 => 200,
		'uploads'       		 => true,
		'flex-height'            => true,
		'flex-width'             => true, 
		'wp-head-callback'       => 'beautyspa_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'beautyspa_custom_header_setup' );

if ( ! function_exists( 'beautyspa_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see beautyspa_custom_header_setup().
*/
	function beautyspa_header_style() {
		$header_text_color = get_header_textcolor();
		$header_image      = get_header_image();
		
		$custom_css = "";
		
	    if ( ! display_header_text() ) {
	    	$custom_css .= "
	            .navbar-brand h1,.navbar-brand h1:hover {
				color: rgba(241, 241, 241, 0);
				position:absolute;
				clip: rect(1px 1px 1px 1px);
				}
				.navbar-header p {
				color: rgba(241, 241, 241, 0);
				position:absolute;
				clip: rect(1px 1px 1px 1px);
				}";
	    } else {
	    	$custom_css .= ".navbar-brand h1, .navbar-header p {
				color: #".esc_attr( $header_text_color ).";
			}";
	    } 
	    if ( has_header_image() ) { 
	    	$custom_css .= ".navbar-default.menu,#top {
	    		background-image: url(".$header_image.");
			}.menu.navbar-default .navbar-nav > li > a{ color: rgba(0, 0, 0, 0.47);}";
	    } 

	    // If we get this far, we have custom styles. Let's do this.
		wp_enqueue_style(
	        'custom-header-style1',
	        get_template_directory_uri() . '/css/custom-header-style.css'
	    );
	    wp_add_inline_style( 'custom-header-style1', $custom_css );	
		?>
	<?php
	}
endif;